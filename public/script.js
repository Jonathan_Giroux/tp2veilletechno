window.onload = function() {
    // Header template
    const headerTemplate = `
    <nav >
    <a class="navbar-brand " href="./index.html">Choisis ton chemin</a>
    <div id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item ">
          <a class="nav-link" href="./presentationProjet.html">Présentation Projet</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./presentationClient.html">Présentation Client</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./defis.html">Défis reliés</a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="./contraintes.html">Présentation Contraintes</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./description.html">Description prosposition</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./reponsedeficontraintes.html">Réponses défis et contraintes</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./schemaMateriel.html">Schématisation matérielle</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./schemaLogicielle.html">Schématisation logicielle</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./choixtechno.html">Choix technologique</a>
        </li>
      </ul>
    </div>
  </nav>
    `;

    // Footer template
    const footerTemplate = `
        <p>William Quimper | Jonathan Giroux | Satya Paul Crane | Phillipe Noël | David-Olivier Jobin</p>
    `;





    // Inserting templates into the placeholders

    document.getElementById('header').innerHTML = headerTemplate;
    document.getElementById('footer').innerHTML = footerTemplate;
}
